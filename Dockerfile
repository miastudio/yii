# Latest compatible version
FROM --platform=linux/amd64 php:7.0.33-apache

# GENERAL UPDATE
RUN apt-get update

# Imagemagick
RUN apt-get install -y imagemagick
# MySQLi
RUN docker-php-ext-install mysqli
# MySQL PDO
RUN docker-php-ext-install pdo_mysql
# gd
RUN apt-get install -y libpng-dev && docker-php-ext-install gd
    ## curl
    #RUN apt-get install -y curl && docker-php-ext-install curl
    #RUN docker-php-ext-install curl
#APCU https://pecl.php.net/package/apcu
#ARG APCU_VERSION=5.1.8
ARG APCU_VERSION=5.1.20
RUN pecl install apcu-${APCU_VERSION} && docker-php-ext-enable apcu
    ##RUN echo "extension=apcu.so" > /usr/local/etc/php/php.ini
    ##RUN echo "apc.enable_cli=1" > /usr/local/etc/php/php.ini
    ##RUN echo "apc.enable=1" > /usr/local/etc/php/php.ini
# Memcache https://pecl.php.net/package/memcached
ARG MEMCACHED_VERSION=3.1.5
RUN apt-get update && apt-get install -y libmemcached-dev zlib1g-dev \
    && pecl install memcached-${MEMCACHED_VERSION} \
    && docker-php-ext-enable memcached
# Memcache ???
#      libmemcached-dev \

# Avoid 'ServerName not set' error
ARG SERVERNAME="Webserver"
RUN echo "ServerName $SERVERNAME" >> /etc/apache2/apache2.conf

# Enable RewriteModule
RUN a2enmod rewrite && \
    service apache2 restart

# Copy custom farmework
COPY . /opt/yii

